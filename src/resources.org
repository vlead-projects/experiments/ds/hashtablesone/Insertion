					#+title:  Contents <HashTable>
			 #+AUTHOR: <Keshavan Seshadri> <Devansh Manu>
					   #+DATE: [2018-05-14 Mon]
			 #+SETUPFILE: ../../org-templates/level-2.org
						#+TAGS: boilerplate(b)
					 #+EXCLUDE_TAGS: boilerplate
						   #+OPTIONS: ^:nil

* Prerequisite
  None

* Introduction
  Hash Table is a data structures that maps keys to values.x
  
* Hash Function
  A function thatmaps large number of keys into a small number of indices.

** Collision Problem
  If a large number of keys  to small number of indices,
  then obviously two or keys may map to the same index by [[https://en.wikipedia.org/wiki/Pigeonhole_principle ][Pigeon Hole Priciple(PHP)]].
  Hash collisions are practically unavoidable when hashing a random subset of a large set
  of possible keys. For example, if 2,450 keys are hashed into a million buckets, even with
  a perfectly uniform random distribution,
  according to the [[https://en.wikipedia.org/wiki/Birthday_problem][birthday problem]] there is approximately a 95% chance of at least two of
  the keys being hashed to the same slot.
  This is the main reason why we need to choose a hash function that maintains data integrity.

** Solving the problem 
   In order to minimize collisions we use the following methods:
   *** Open Addressing
    **** [[https://en.wikipedia.org/wiki/Linear_probing][Linear Probing]]
    **** [[https://en.wikipedia.org/wiki/Quadratic_probing][Quadratic Probing]]
    **** [[https://en.wikipedia.org/wiki/Double_hashing][Double Hashing]]
   *** Closed Addressing
	**** [[][Separate Chaining]]

** Choosing a Hash Function
   A perfect hash function ensures that tere are minimal or NO collisons.
  
*** Load Factor
	A critical statistic for a hash table is the load factor, defined as

					\(load factor =\frac{x}{n}\)

	where
    n is the number of entries occupied in the hash table.
    k is the number of buckets.

** 
